## Themes of  lecture

### CSS
- Avoiding duplication
- BEM modifiers

### JS (ES6)
- Event listeners: ``` onclick, onmouseenter, onmouseover ...```
- XMLHttpRequest's: ``` $.ajax```
- ES6 modules
- Class
- Keywords: ``` constructor(), super(), this ```
- Array methods: ``` map(), filter(), forEach() ```
- Spread / Rest operators ``` [...array] ```
- Dynamic content with ``` $.ajax ```

### Backend (Wordpress)
- Coockies
- Authorization
- Request headers
- Redirects
- Personal dashboard development
